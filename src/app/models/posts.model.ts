export interface PostsModel {
  id: number;
  author: string;
  title: string;
}
