import {Action, createAction} from '@ngrx/store';

export const LOAD_PIZZAS = createAction('[Products] Load Pizzas');
export const LOAD_PIZZAS_FAIL = createAction('[Products] Load Pizzas Fail');
export const LOAD_PIZZAS_SUCCESS = createAction('[Products] Load Pizzas Success');

export class LoadPizzas implements Action {
   type = LOAD_PIZZAS;
}

export class LoadPizzasFail implements Action {
   type = LOAD_PIZZAS_FAIL;
}

export class LoadPizzasSuccess implements Action {
   type = LOAD_PIZZAS_SUCCESS;
}
