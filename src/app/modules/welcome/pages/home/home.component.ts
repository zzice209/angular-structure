import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../../core/services/api.service';
import {urls} from '@consts/urls';
import {from, Observable, of} from 'rxjs';
import {map, pluck, switchMap, tap} from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { increment, decrement, reset, duplicate, half } from '@actions/counter.action';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit {
  results$: Observable<any>;
  name$: Observable<any>;
  count$: Observable<number>;

  constructor(
    private apiService: ApiService,
    private store: Store<{count: number}>
  ) {
    this.count$ = store.select('count');
  }
  ngOnInit(): void {
    // this.results$ = this.apiService.get(urls.GET_POKEMON)
    //   .pipe(
    //     map(data => from(data.results)),
    //     // switchMap(response => of(response)),
    //     tap(data => console.log(data))
    //   );
    //
    // this.results$.pipe(pluck('name')).subscribe((data) => console.log(data));
    //
    // const source = from([
    //   { name: 'Joe', age: 30 },
    //   { name: 'Sarah', age: 35 }
    // ]);
    // console.log('source', source);
  }

  increment(): void {
    this.store.dispatch(increment());
  }

  decrement(): void {
    this.store.dispatch(decrement());
  }

  duplicate(): void {
    this.store.dispatch(duplicate());
  }

  half(): void {
    this.store.dispatch(half());
  }

  reset(): void {
    this.store.dispatch(reset());
  }

}
