import {createReducer, on} from '@ngrx/store';
export const initialState = 0;

const _counterReducer = createReducer(
  initialState,
  on(increment, (state) => state + 1),
  on(decrement, (state) => state - 1),
  on(reset, (state) => state = 0)
);

export function rootReducers(state, action): any {
  return _counterReducer(state, action);
}
