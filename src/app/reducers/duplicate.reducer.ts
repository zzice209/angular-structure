import {createReducer, on} from '@ngrx/store';
import { duplicate, half, reset } from '@actions/counter.action';

export const initialState = 0;

const _duplicateReducer = createReducer(
  initialState,
  on(duplicate, (state) => state * 2),
  on(half, (state) => state / 2),
);

export function duplicateReducer(state, action): any {
  return _duplicateReducer(state, action);
}
